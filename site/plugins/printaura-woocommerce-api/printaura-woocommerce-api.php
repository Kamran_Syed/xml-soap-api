<?php 
/*
  Plugin Name: PrintAura WooCommerce API
  Plugin URI: https://printaura.com
  Description: PrintAura  WooCommerce Integration API
  Author: Print Aura
  Version: 3.1.0
  Author URI: http://printaura.com
*/

  // Turn on debugging?
define('WC_JSON_API_DEBUG',false);

define( 'REDE_PLUGIN_BASE_PATH', plugin_dir_path(__FILE__) );
if (! defined('REDENOTSET')) {
  define( 'REDENOTSET','__RED_E_NOTSET__' ); // because sometimes false, 0 etc are
  // exspected but consistently dealing with these situations is tiresome.
}
    


 if(!version_compare(phpversion(),'5.3','<')){
require_once( plugin_dir_path(__FILE__) . 'classes/class-rede-helpers.php' );
require_once( plugin_dir_path(__FILE__) . 'classes/class-pa-updater-config.php' );
require_once( plugin_dir_path(__FILE__) . 'classes/class-pa-updater.php' );
require_once( REDE_PLUGIN_BASE_PATH . '/printaura-woocommerce-api-synchronize.php' );
require_once( plugin_dir_path(__FILE__) . 'printaura-woocommerce-api-core.php' );
}




function add_shipped_order_woocommerce_email( $email_classes ) {

	// include our custom email class
	require_once( plugin_dir_path(__FILE__) .'classes/class-wc-shipped-order-email.php' );

	// add the email class to the list of email classes that WooCommerce loads
	$email_classes['WC_Shipped_Order_Email'] = new WC_Shipped_Order_Email();

	return $email_classes;

}
function add_woo_email(){

}
function send_ship($order_id){
  
    $active_plugins = (array) get_option( 'active_plugins', array() );
  if(in_array('woocommerce/woocommerce.php', $active_plugins)){
      //$order = new WC_Order( $order_id );
      $email = new WC_Emails();
      $emails = $email->get_emails();
      $sh_email= $emails['WC_Shipped_Order_Email'];
      $sh_email->trigger($order_id);
     
  }
    
    
}

function printaura_woocommerce_api_initialize_plugin() {
  $helpers = new JSONAPIHelpers();
  require_once( REDE_PLUGIN_BASE_PATH . '/printaura-woocommerce-api-actions.php' );

} // end woocommerce_json_api_initialize_plugin
add_action('init', 'print_aura_updater');
function print_aura_updater()
{
    
    //var_dump(get_plugin_data());
	$args = array(
            'plugin_name' => 'Print Aura API',                  // Your plugin name (e.g. "Soliloquy" or "Jetpack")
            'plugin_slug' => 'printaura-woocommerce-api',                  // Your plugin slug (typically the plugin folder name, e.g. "soliloquy")
            'plugin_path' => plugin_basename( __FILE__ ),  // The plugin basename (e.g. plugin_basename( __FILE__ ))
            'plugin_url'  => WP_PLUGIN_URL . '/printaura-woocommerce-api', // The HTTP URL to the plugin (e.g. WP_PLUGIN_URL . '/soliloquy')
            'version'     => '3.1.0',                      // The current version of your plugin
            'remote_url'  => 'https://printaura.com/printaura-woocommerce-updater/',    // The remote API URL that should be pinged when retrieving plugin update info
            'time'        => 42300                         // The amount of time between update checks (defaults to 12 hours)
        );
        $config            = new TGM_Updater_Config( $args );
        $namespace_updater = new TGM_Updater( $config ); // Be sure to replace "namespace" with your own custom namespace
        $namespace_updater->update_plugins();
	
}
function pa_disabled_notice(){
     echo '<div id="error" class="error"><br /><strong>Print Aura Woocommere API Plugin requires PHP version 5.3 or higher. Please click <a href="https://printaura.com/woocommerce-server-requirements/" target="_blank"> here</a> for more information about server requirements.</strong><br /><br /></div>';
}
function check_version(){
    if(version_compare(phpversion(),'5.3','<')){
         if ( is_plugin_active( plugin_basename( __FILE__ ) ) ) {
             deactivate_plugins( plugin_basename( __FILE__ ) ); 
             add_action( 'admin_notices','pa_disabled_notice'  );
              if ( isset( $_GET['activate'] ) ) {
	                    unset( $_GET['activate'] );
	                }
         }
      
	        
  }
}
function printaura_woocommerce_api_activate() {
  global $wpdb;
   if(version_compare(phpversion(),'5.3','<')){
      deactivate_plugins( plugin_basename( __FILE__ ) );  
      die( __( 'Print Aura Woocommere API Plugin requires PHP version 5.3 or higher. Please click <a href="https://printaura.com/woocommerce-server-requirements/" target="_blank"> here</a> for more information about server requirements.', 'printaura_api' ) );
	        
  }
  $helpers = new JSONAPIHelpers();
  $current_user 	= wp_get_current_user();
  $user_id	= $current_user->ID;
  wp_insert_term('shipped','shop_order_status');
  wp_insert_term('partially shipped','shop_order_status');
  wp_insert_term('T-shirts','product_shipping_class');
  wp_insert_term('Sweatshirts','product_shipping_class');
  $key = $helpers->getPluginPrefix() . '_enabled';
  $key1 = $helpers->getPluginPrefix() . '_token';
  $key2 = $helpers->getPluginPrefix() . '_ips_allowed';
  $from = $helpers->getPluginPrefix() . '_mail_from';
  $subject = $helpers->getPluginPrefix() . '_mail_subject';
  $body = $helpers->getPluginPrefix() . '_mail_body';
  $from_default="From: {Sender} <{EMAIL}>";
  $subject_default = "Ship Notification: #{ORDERNUMBER}";
  $body_default  = '<p>Dear {CustomerFirstName},</p>
<p>The following items have shipped from order #{ORDERNUMBER}</p>

{CLIENT PRODUCT TITLE} - {COLOR} / {SIZE} (Qty: {QTY})
</p>
<p>
The order has been shipped to:
</p>
<p>
{SHIP_ADDRESS}
</p>
<p>via {SHIP_METHOD} (Tracking #: {TRACKINGNUMBER} )</p>
<p>Please note that it may take until the next business day before tracking becomes available.</p>

Thanks for your business.';
  $apiKey = wp_hash_password(date("YmdHis",time()) . rand(1000,99999) . $_SERVER['REMOTE_ADDR'] . SECURE_AUTH_SALT . $user_id);

  update_option($key,'yes');
  if(!get_option($key1)){
    update_option($key1,$apiKey);
    update_user_meta($user_id,$key1,$apiKey);
  }
  update_option($key2,'162.209.60.177');
  update_option($from,$from_default);
  update_option($subject,$subject_default);
  update_option($body,$body_default);
  
  
}

function printaura_woocommerce_api_deactivate() {
  global $wpdb;
  $status1=get_term_by('name','shipped','shop_order_status');
  $status2=get_term_by('name','partially shipped','shop_order_status');
  wp_delete_term(intval($status2->term_id),'shop_order_status');
  wp_delete_term(intval($status1->term_id),'shop_order_status');
}

add_action( 'admin_init', 'woo_init' );
function woo_init() {
    if ( current_user_can( 'delete_posts' ) )
        add_action( 'delete_post', 'woo_delete_product', 10 );
    // if ( current_user_can( 'save_post' ) )
       add_action( 'save_post', 'woo_update_product' );
}
register_activation_hook( __FILE__, 'printaura_woocommerce_api_activate' );
register_deactivation_hook( __FILE__, 'printaura_woocommerce_api_deactivate' );
add_action( 'admin_init', 'check_version'  );
// I am hoping this will make the plugin be the last to be initialized
add_action( 'init', 'printaura_woocommerce_api_initialize_plugin',5000 );

