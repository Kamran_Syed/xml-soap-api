<style type="text/css">
    .orange{
        color:#EF7F2C;
    }
    .submit_btn {
        background-color: #21759B;
        color:#FFFFFF;
        
    }
</style>
<div class="form-table" style="width:800px;">
<h1>Print Aura - App Options</h1>
    <p>
 This plugin is designed to allow you to sell products such as t-shirts and other products and have them automatically printed/shipped on demand
by Print Aura with no minimum order. To view more about the Print Aura Woocommerce app and how it works  please visit
<a href="https://printaura.com/woocommerce-app/"  class="orange" target="_blank">printaura.com/woocommerce-app</a>.
<br /><br />
Once you have authorized the app on PrintAura.com you will need to add products from printaura.com.
<br /><br />
• <a href="https://printaura.com/add-products/" class="orange" target="_blank">Add products</a>
<br />
• <a href="https://printaura.com/manage-products/" class="orange" target="_blank">Manage products</a>

    </p>

<?php
$max_upload = (int)(ini_get('upload_max_filesize'));
$max_post = (int)(ini_get('post_max_size'));
$upl = wp_upload_dir() ;
$upload_dir = $upl['path'];
$is_writable = is_writable($upload_dir);
$color=($is_writable) ? "<font color='green'> Looks good! This folder is writable</font>." : "<font color='red'>is not-writable, Please visit <a href='http://codex.wordpress.org/Changing_File_Permissions'  target='_blank'> this tutorial</a> to change your file permissions</font>";
?>
<h3>Server Settings </h3>
To make sure the app is working correctly and images will display in your store please check the settings below:<br />
<h4>File Size Limit</h4>
In order to upload your files you need to make sure that your server is set to allow uploads of a certain size. That number will
be your limitation<strong> of mockup images</strong> you can use. To update your upload size please view our <a class="orange" href="https://printaura.com/change-upload-limits/" target='_blank'> tutorial on changing upload limits</a>.

Current MAX upload file size: <?php echo $max_upload; ?>MB
<h4>File Permissions</h4>
We need to make sure that we can upload your mockup images to display in your store. 
<br />
Upload directory (<?php echo $upload_dir; ?>)<?php echo $color;?>.
<br />
<h3>API Settings</h3>
<p>
The following settings are related to the Print Aura API plugin. 
</p>
<form method="post" id="mainform" action="" >
<?php echo $helpers->hiddenFormFields($helpers->getPluginPrefix() . '_sitewide_settings'); ?>
<table class="form-table">
 <?php
  foreach ($attrs['json_api_sitewide_settings']['fields'] as $value) {
    ?>
      <tr>
        <td width="40%" valign="top">
         <strong> <?php echo $helpers->labelTag($value) ?></strong><br />
          <?php echo $value['description']?>
        </td>
        <td>
         <?php 
            if ( $helpers->orEq($value,'type','') == 'text') { 
              echo $helpers->inputTag( $value ); 
            } else if ( $helpers->orEq($value,'type','') == 'textarea' ) {
              echo $helpers->textAreaTag( $value ); 
            } else if ( $helpers->orEq( $value, 'type', '') == 'select') {
              echo $helpers->selectTag( $value );
            }
         ?> 
        </td>
      </tr>
     
    <?php
  }
?>
  <tr>
    <td colspan="2" align="left">
      
      <input type="submit" class="button button-primary" name="submit" value="<?php _e('Save Changes') ?>" />
    </td>
  </tr>
</table>
</form>
</div>
