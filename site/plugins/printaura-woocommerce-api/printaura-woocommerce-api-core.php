<?php 
/**
 * Add fields to the user profile that allow the addition 
 * of a "token" that can be used by the API to log that
 * user into the system for performing actions.
 * 
 * The API is not limited to admins, in fact, the general idea
 * is to limit the API by applying whatever limits apply
 * to the user.
 * 
 * @param $user the user
*/
require_once( plugin_dir_path(__FILE__) . 'classes/class-wc-json-api.php' );

/*
  Prevent template code from loading :)
*/
function print_aura_api_template_redirect() {
  global $wpdb;
  $helpers = new JSONAPIHelpers();

  $headers = print_aura_api_parse_headers();
  if ( isset($headers['Content-Type']) && $headers['Content-Type'] == 'application/json') {
    $fp = @fopen('php://input','r');
    $body = '';
    if ($fp) {
      while ( !feof($fp) ) {
        $buf = fread( $fp,1024 );
        if (is_string( $buf )) {
          $body .= $buf;
        }
      }
      fclose( $fp );
    }
    $hash = json_decode( $body, true );
    foreach ( $hash as $key => $value ) {
      $_REQUEST[$key] = $value;
    }
  }
  if (!isset($_REQUEST['action']) || $_REQUEST['action'] != 'print_aura_api') {
      return;
  }
  if (is_user_logged_in()) {
    return;
  }
  

  JSONAPIHelpers::debug( var_export( $headers, true) );
  if ( isset( $_REQUEST['action'] ) && 'print_aura_api' == $_REQUEST['action']) {
    $enabled = get_option( $helpers->getPluginPrefix() . '_enabled');
    $require_https = get_option( $helpers->getPluginPrefix() . '_require_https' );
    if ( $enabled != 'no') {
      if ( $require_https == 'yes' && $helpers->isHTTPS() == false ) {
        JSONAPIHelpers::debug("Cannot continue, HTTPS is required.");
        return;
      }
      if ( defined('WC_JSON_API_DEBUG') ) {
        JSONAPIHelpers::truncateDebug();
      }
      $api = new WooCommerce_JSON_API();
      $api->setOut('HTTP');
      $api->setUser(null);
      $params = array();
      // maybe we had to serialize some subarrays, so we'll have to unserialize them here
      foreach ($_REQUEST as $key=>$value) {
        $params[$key] = $value;
      }
      foreach ( array('payload','arguments','model_filters','wordpress_filters') as $key ) {
        if ( isset($_REQUEST[$key]) && is_string($_REQUEST[$key]) ) {
          $params[$key] = json_decode(stripslashes($_REQUEST[$key]),true);
        }
      }
      $api->route($params);

    } else {
      JSONAPIHelpers::debug("JSON API is not set to enabled.");
    }
  }
}
function admin_css(){
   wp_enqueue_style( 'fixed-style', plugins_url( '/css/style.css',__FILE__) );
}
function print_aura_api_admin_menu() {
  global $menu;
 
    add_menu_page( 'Print Aura Woocommerce API',
          'Print Aura API', 
          'manage_woocommerce', 
          'print_aura_api_settings_page', 'print_aura_api_settings_page');
    
}

function print_aura_api_settings_page() {

  $helpers = new JSONAPIHelpers();
  $current_user=wp_get_current_user();

  $key5 = $helpers->getPluginPrefix() . '_enabled';
  $key3 = $helpers->getPluginPrefix() . '_token';
  $key4 = $helpers->getPluginPrefix() . '_ips_allowed';
  $params = $_POST;
  $nonce = $helpers->orEq( $params, '_wpnonce',false);
  $key = $helpers->getPluginPrefix() . '_sitewide_settings';
  if ( $nonce  && wp_verify_nonce( $nonce, $helpers->getPluginPrefix() . '_sitewide_settings' ) && isset($params[$key]) ) { 
    foreach ($params[$key] as $key2=>$value) {
      update_option($helpers->getPluginPrefix() . '_' . $key2, maybe_serialize($value));
    }
  
  update_option($key3,$_POST[$key]['token']); 
  update_user_meta($current_user->ID,$key3,$_POST[$key]['token']);  
  update_option($key4,$_POST[$key]['ips_allowed']);  
  update_option($key5,$_POST[$key]['enabled']);  
  }
  
  
  $attrs = array (
	  'json_api_sitewide_settings' => array(
		  'title' => __( 'Print Aura Woocommerce API Settings', 'print_aura_api' ),
		  'fields' => array(
          array(
            'name'          => $helpers->getPluginPrefix() . '_sitewide_settings[enabled]',
            'id'            => 'json_api_enabled_id',
            'value'         => get_option( $helpers->getPluginPrefix() . '_enabled' ),
            'options'       => array(
                array( 'value' => 'yes', 'content' => __('Yes','print_aura_api')),
                array( 'value' => 'no', 'content' => __('No','print_aura_api')),
            ),
            'type'          => 'select',
            'label'         => __( 'API Enabled?', 'print_aura_api' ),
            'description'   => __('Quickly enable/disable The API', 'print_aura_api' ),
          ),
          array(
            'name'          => $helpers->getPluginPrefix() . '_sitewide_settings[token]',
            'id'            => 'json_api_token_id',
            'value'         => get_option($helpers->getPluginPrefix() . '_token'),
            'type'          => 'text',
            'label'         => __( 'API Token', 'print_aura_api' ),
            'description'   => __('You will need to enter this on<br />
<a class="orange" href="https://printaura.com/woocommerce/" target="_blank">https://printaura.com/woocommerce</a> to setup
the app', 'print_aura_api' )
          )            
           ),
	  ),
	);
  
  $attrs = apply_filters('print_aura_api_sitewide_settings_fields', $attrs);
  
  echo $helpers->renderTemplate('admin-settings-page.php', array( 'attrs' => $attrs) );
}

function print_aura_api_parse_headers() {
  $headers = array();
  foreach ( $_SERVER as $key=>$value) {
    if ( substr($key,0,5) == 'HTTP_' ) {
      continue;
    }
    $h = str_replace(' ', '-', ucwords( str_replace('_', ' ', strtolower( $key ) ) ) );
    $headers[$h] = $value;
  }
  return $headers;
}
 
function unhook_those_pesky_emails( $email_class ) {
    remove_action('woocommerce_order_status_completed_notification', array(&$email_class, 'customer_completed_order'));
}
