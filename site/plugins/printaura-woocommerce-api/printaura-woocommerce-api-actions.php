<?php
add_action('admin_menu','print_aura_api_admin_menu',10);
// these will only fire if you use the admin-ajax.php url. which is a bitch to use.
add_action('wp_ajax_print_aura_api','print_aura_api_template_redirect');
add_action('wp_ajax_nopriv_print_aura_api','print_aura_api_template_redirect');
//This one does seem to work.
add_action('wp_loaded','print_aura_api_template_redirect');
add_action('admin_head','admin_css');
//add_action( 'woocommerce_email', 'unhook_complete_order_email' );
add_filter( 'woocommerce_email_classes', 'add_shipped_order_woocommerce_email' );
add_action( 'woocommerce_order_status_shipped','send_ship');
add_action( 'woocommerce_order_status_processing','add_pa_order');
//add_action( 'woocommerce_email','add_woo_email');
