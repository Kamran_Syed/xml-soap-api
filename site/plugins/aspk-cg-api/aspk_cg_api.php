<?php
/*
Plugin Name: ASPK CG API 
Plugin URI: 
Description: It integrates Woocommerce with CG fulfilment.
Author: Agile Solutions pk
Version: 1.1
Author URI: http://agilesolutionspk.com
*/

include_once __DIR__ .'/aspk_api.php';
if ( !class_exists( 'ASPK_CG_API' )){
	class ASPK_CG_API{

		function __construct(){
			add_action( 'woocommerce_order_status_processing', array(&$this,'integrate_api' ), 10);
			//add_action( 'woocommerce_order_status_completed', array(&$this,'integrate_api' ), 10);
			add_action('admin_menu', array(&$this, 'admin_menu'));
		}
		
		function admin_menu(){
			add_options_page( 'Soap Api Setting', 'Soap Api Setting', 'manage_options', 'soap_settings', array(&$this, 'save_soap_setting'));
		}
		
		function soap_settings_default(){
			$defatuls = array();
			$defatuls['soap_end_point'] = '';
			$defatuls['soap_access_token'] = '';
			$defatuls['soap_customer_id'] = '';
			$defatuls['soap_enable_trace'] = '';
			return $defatuls;
		}
		
		function get_settings(){
			$defatuls = $this->soap_settings_default();
			$saved_data = get_option('_save_soap_setting',$defatuls);
			return $saved_data;
		}
		function save_soap_setting(){
			$rets_settings = array();
			if(isset($_POST['save_soap_setting'])){
				$soap_end_point = $_POST['soap_end_point'];
				$soap_access_token = $_POST['soap_access_token'];
				$soap_customer_id = $_POST['soap_customer_id'];
				$soap_enable_trace = $_POST['soap_enable_trace'];
				$rets_settings['soap_end_point'] = $soap_end_point;
				$rets_settings['soap_access_token'] = $soap_access_token;
				$rets_settings['soap_customer_id'] = $soap_customer_id;
				$rets_settings['soap_enable_trace'] = $soap_enable_trace;
				//$this->aspk_logs($rets_settings,'SOAP API Settings');
				update_option('_save_soap_setting',$rets_settings);
			}
			$defatuls = $this->soap_settings_default();
			$saved_data = get_option('_save_soap_setting',$defatuls);
			?>
				<div style="float:left;clear:left;padding:3em;background-color:#FFFFFF;margin-top: 1em;"> 
					<?php
					if(isset($_POST['save_soap_setting'])){
						?>
						<div id="rets_save_message" class="updated" style="float:left;clear:left;">
							Settings have been saved
						</div>
						<?php
					}
					?>
					<div style="float:left;clear:left;">
						<h3> SOAP API Settings </h3>
					</div>
					<div style="float:left;clear:left;">
						<form action="" method="post">
							<div style="float:left;clear:left;margin-top:1em;">
								<div style="float:left;width: 6em;">End Point</div>
								<div style="margin-left:1em;float:left;"><input value="<?php if(isset($saved_data['soap_end_point'])){echo $saved_data['soap_end_point'];}?>" required style="width: 20em;" type="url" name="soap_end_point"/> </div>
							</div>
							<div style="float:left;clear:left; margin-top:1em;">
								<div style="float:left;width: 6em;">Access token</div>
								<div style="margin-left:1em;float:left;"><input value="<?php if(isset($saved_data['soap_access_token'])){echo $saved_data['soap_access_token'];}?>" required style="width: 20em;"  type="text" name="soap_access_token"/> </div>
							</div>
							<div style="float:left;clear:left; margin-top:1em;">
								<div style="float:left;width: 6em;">CustomerID</div>
								<div style="margin-left:1em;float:left;"><input value="<?php if(isset($saved_data['soap_customer_id'])){echo $saved_data['soap_customer_id'];}?>" required style="width: 20em;"  type="number" name="soap_customer_id"/> </div>
							</div>
							<div style="float:left;clear:left;margin-top:1em;">
								<div style="float:left;width: 6em;">Enable Trace</div>
								<div style="margin-left:1em;float:left;"><input  <?php checked( $saved_data['soap_enable_trace'], 'yes'); ?> type="checkbox" name="soap_enable_trace" value="yes"/> </div>
							</div>
							<div style="float:left;clear:left;margin-top:1em;">
								<div style="float:left;">
									<input class="button button-primary" type="submit" name="save_soap_setting" value="Save"/>
								</div>
							</div>
						</form>
					</div>
				</div>
				<script>
					setTimeout(function(){ 
						jQuery('#rets_save_message').hide();
					}, 8000);
				</script>
			<?php
		}
		function aspk_logs($params, $setting){ // $params should be array
			$params ['Soap settings']= $setting;
			ob_start();
			$params = array_reverse($params);
			print_r($params);
			$get_content = ob_get_contents();
			file_put_contents(ABSPATH. "wc-logs/aspk-cg-api.log",date('Y-m-d H:i:s').'   '.$get_content.PHP_EOL,FILE_APPEND);
		}
		
		function log_xml($xml){
			file_put_contents(ABSPATH. "wc-logs/aspk-cg-xml.log",date('Y-m-d H:i:s').'   '.$xml.PHP_EOL,FILE_APPEND);
		}
		
		
		function integrate_api( $order_id){
			$saved_data = $this->get_settings();
			$aspk_order_detail = new OrderDetailsList($order_id);
			$order = new Order();
			$order_obj = new WC_Order( $order_id ); 
			$order->CustomerID = $saved_data['soap_customer_id'];
			$order->ClientPO = $order_id;
			$order->OrderDetailsList = $aspk_order_detail;
			if(empty($aspk_order_detail->OrderDetails)) return;
			$params = array(
			  "AccessToken" => $saved_data['soap_access_token'],
			  "Order" => $order
			); 
			
			try{
				$client = new SoapClient($saved_data['soap_end_point'], array('trace' => 1));
				$response = $client->__soapCall("OrderCreate", array($params));
				$this->log_xml($client->__getLastRequest());
				
			}catch(SoapFault $e){
				file_put_contents(ABSPATH. "wc-logs/aspk-cg-api.log",date('Y-m-d H:i:s').' Fetal Error  '.$e->getMessage().PHP_EOL,FILE_APPEND);
				$this->log_xml($client->__getLastRequest());
				$order_obj->add_order_note( __('Order status still on-hold.CG API Response'.$e->getMessage(),'woothemes' ) );
				$order_obj->update_status('on-hold', 'order_note');
				return;
				
			}
			$get_response['API Response'] = $response;
			if($saved_data['soap_enable_trace'] == 'yes'){
				$this->aspk_logs($params,'SOAP API Request');
				$this->aspk_logs($get_response,'Response');
			}
			$order_obj->add_order_note( __('CG API Response'.$response->OrderCreateResult,'woothemes' ) );
		}
		
	} //class ends
} //class exists ends
$aspk_cg_api = new ASPK_CG_API();
?>
