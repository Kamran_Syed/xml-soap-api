<?php 

class OrderDetailsList{
	public $OrderDetails;
	
    function OrderDetailsList($order_id) {
		$this->OrderDetails = array();
		
		$order = new WC_Order( $order_id );
		$get_item = $order->get_items(); //get items
		foreach($get_item as $item){
			$tmp = new OrderDetails();
			$pid = $item['product_id'];
			$category = wp_get_post_terms(  $pid, 'product_cat' );
			
			foreach($category as $cg){
				if($cg->slug == 'cg'){ 
					$product_variation_id = $item['variation_id'];
					
					// Check if product have variation.
					  if ($product_variation_id) { 
						  $pd = new WC_Product_Variable($item['variation_id']);
					  } else {
						  $pd = new WC_Product($item['product_id']);
					  }

					  // Output SKU
					  $sku = $pd->get_sku();
				
					$tmp->Customer_SKU = $sku;
					$tmp->Qty = $item['qty'];
					if($order->shipping_method_title == 'Ground (6-9 days CONUS)'){
						$tmp->ShipMethod  = 60;
					}else{
						$tmp->ShipMethod  = $order->shipping_method;
					}
					$tmp->ShipTo  = $order->shipping_first_name.' '.$order->shipping_last_name;
					$tmp->ShipToAddress1  = $order->shipping_address_1;
					$tmp->ShipToCity  = $order->shipping_city;
					$tmp->ShipToState  = $order->shipping_state;
					$tmp->ShipToZip  = $order->shipping_postcode;
					$tmp->ShipToCountry  = $order->shipping_country;
					$tmp->ShipToEmail   = $order->billing_email;
					$tmp->ShipToPhone    = $order->billing_phone;
					$this->OrderDetails[] = $tmp;
				}
			}
		}
		
    }
}

class Order extends stdClass{
    function Order() {
		
    }
}

class OrderDetails extends stdClass{
    function OrderDetails() {
		
    }
}







